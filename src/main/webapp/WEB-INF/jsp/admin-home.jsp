<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>后台系统管理</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>

</head>
<body>
<%--<%--%>
<%--	Reader reader = (Reader)session.getAttribute("admin");--%>
<%--%>--%>
    <!-- 顶部开始 -->
    <div class="container">
        <div class="logo"><a href="index.html">图书管理系统</a></div>
        <div class="left_open">
            <i title="展开左侧栏" class="iconfont">&#xe699;</i>
        </div>  
        <ul class="layui-nav right" lay-filter="">
        	<li class="layui-nav-item">
        		<a href="#">欢迎你,管理员：${session_reader.readerName}</a>
        	</li>
        	<li class="layui-nav-item">
        		<a href="/user/back.do">退出登录</a>
        	</li>
        </ul>
        
    </div>
    <!-- 顶部结束 -->
    <!-- 中部开始 -->
     <!-- 左侧菜单开始 -->
    <div class="left-nav">
      <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>个人管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="/resetList">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>修改信息</cite>
                            
                        </a>
                    </li >
                    <li>
                        <a _href="/user/deletReader">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>注销账户</cite>

                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe723;</i>
                    <cite>用户管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="/admin/userList">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>用户列表</cite>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe726;</i>
                    <cite>图书管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="/admin/returnAddBook">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>图书上架</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="/admin/checkBookList">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>审核借书</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="/admin/returnBookList">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>审核还书</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="/admin/bookList">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>图书库存</cite>
                        </a>
                    </li >
                </ul>
            </li>           
        </ul>
      </div>
    </div>
    <!-- <div class="x-slide_left"></div> -->
    <!-- 左侧菜单结束 -->
    <!-- 右侧主体开始 -->
    <div class="page-content">
        <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
          <ul class="layui-tab-title">
            <li>我的桌面</li>
          </ul>
          <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='/welcome.do' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
          </div>
        </div>
    </div>
    <div class="page-content-bg"></div>
    <!-- 右侧主体结束 -->
    <!-- 中部结束 -->
    <!-- 底部开始 -->
    <div class="footer">
        <div class="copyright">Copyright ©2019 第一小组</div>
    </div>
    <!-- 底部结束 -->
    
</body>
</html>