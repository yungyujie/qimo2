<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
      <meta name="renderer" content="webkit">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
  </head>
  
  <body>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>IBSN</th>
            <th>图书名称</th>
            <th>读者ID</th>
            <th>读者名称</th>
            <th>借书日期</th>
            <th>还书日期</th>
            <th>剩余数量</th>
            <th>当前状态</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${bookList}" var="book" >
          <tr>
              <td>${book.ISBN}</td>
              <td>${book.bookName}</td>
              <td>${book.readerID}</td>
              <td>${book.readerName}</td>
              <td>${book.borrowDate}</td>
              <td>${book.returnDate}</td>
              <td>${book.count}</td>
              <td>${book.state}</td>
            <td>
            	<a title="通过审核" href="/admin/checkBook?bid=${book.ISBN}&uid=${book.readerID}">
                <i class="layui-icon">&#xe618;</i>通过审核
        		</a>
        		<a title="取消借阅" style="margin-left: 20px" href="/admin/cancelBook?bid=${book.ISBN}&uid=${book.readerID}">
                <i class="layui-icon">&#xe640;</i>取消借阅
              </a>
            </td>            
          </tr>
<%--          <%--%>
<%--        		}--%>
<%--        	}--%>
<%--          %>--%>
        </c:forEach>
        </tbody>
      </table>
    </div>   
  </body>
</html>