<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>用户列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
  </head>
  
  <body>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>用户ID</th>
            <th>用户姓名</th>
            <th>用户系别</th>
            <th>用户电话</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${reader}" var="rd" >
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td>${rd.readerId}</td>
            <td>${rd.readerName}</td>
            <td>${rd.readerDept}</td>
            <td>${rd.readerPhone}</td>
            <td class="td-manage">
              <a title="修改信息" href="/admin/sendId?id=${rd.readerId}&name=${rd.readerName}&dept=${rd.readerDept}&phone=${rd.readerPhone}">
                <i class="layui-icon">&#xe618;</i>修改信息
              </a>
              <a title="删除用户" style="margin-left: 20px;" href="/admin/delUser?id=${rd.readerId}">
                <i class="layui-icon">&#xe640;</i>删除用户
              </a>
              <a title="设为管理员" style="margin-left: 20px;" href="/admin/addAdmin?id=${rd.readerId}">
                <i class="layui-icon">&#xe642;</i>设为管理员
              </a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>   
  </body>
</html>