package com.sptpc.dao;

import com.sptpc.domain.Book;
import com.sptpc.domain.BorrowBook;
import com.sptpc.domain.po.BookState;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface BookMapper {
	List<Book> selectAll();

	List<Book> selectBookByName(String bookName);

	int bookingById(@Param("uid") String uid, @Param("bid") String bid,
					@Param("startDate") Date startDate, @Param("endDate")Date endDate);

	List<BookState> queryBorrowById(String uid);

	List<BookState> queryReadingListByid(String uid);

    BorrowBook queryDateByUidAndBid(@Param("uid") String uid, @Param("bid") String bid);

	int updateBk(BorrowBook borrowBook);

    int returnBookById(@Param("uid") String uid, @Param("bid") String bid, @Param("date") Date date);

	List<BookState> queryReturnStateById(String uid);

	List<BookState> queryReturnedById(String uid);

    List<BookState> queryReturnBookList();

	List<BookState> queryAllBookState();

    List<BookState> selectBookStateByName(String bookName);

    Book selectCountByBid(String bid);
}
