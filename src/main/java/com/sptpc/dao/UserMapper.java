package com.sptpc.dao;

import com.sptpc.domain.BookReview;
import com.sptpc.domain.Reader;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
	Reader findUserByIdAndPwd(@Param("id") String id, @Param("password") String password);

	int updateAllById(Reader reader);

	int updatePwdById(@Param("newpass") String newpass, @Param("id") String id , @Param("pass") String pass);

	int addReader(Reader reader);

    int delReaderByUidAndPwd(@Param("uid") String uid, @Param("pwd") String pwd);

    int review(@Param("bid") String bid,@Param("uid") String uid,@Param("grade") String grade,@Param("desc") String desc);

	List<BookReview> queryAllReview();

    List<BookReview> searchReview(String bookName);

    List<BookReview> queryReviewById(String uid);
}
