package com.sptpc.dao;

import com.sptpc.domain.Book;
import com.sptpc.domain.Reader;
import com.sptpc.domain.po.BookState;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminMapper {
    Reader findUserByIdAndPwd(@Param("id") String id, @Param("password") String password);

    List<Reader> findAll();

    List<BookState> queryCheckBookList();

    int borrowBook(@Param("bid") String bid, @Param("uid") String uid);

    int minusBook(String bid);

    int cancelBook(@Param("uid") String uid, @Param("bid") String bid);

    int addBook(Book book);

    int passReturn(@Param("bid") String bid, @Param("uid") String uid);

    int reset(@Param("id") String id, @Param("name") String name, @Param("dept") String dept, @Param("phone") String phone);

    int delUserById(String id);

    int addAdmin(String id);
}
