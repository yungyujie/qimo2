package com.sptpc.service;

import com.sptpc.domain.BookReview;
import com.sptpc.domain.Reader;

import java.util.List;

public interface UserService {
	Reader findUserByIdAndPwd(String id, String password);

	int updateAllById(Reader reader);

	int updatePwdById(String newpass, String id , String pass);

	int addReader(Reader reader);

	int delReaderByUidAndPwd(String uid, String pwd);//删除用户通过id和密码

    int review(String bid, String uid, String grade, String desc);

    List<BookReview> queryAllReview();

    List<BookReview> searchReview(String bookName);

    List<BookReview> queryReviewById(String uid);
}
