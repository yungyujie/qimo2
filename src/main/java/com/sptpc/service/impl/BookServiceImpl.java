package com.sptpc.service.impl;

import com.sptpc.dao.BookMapper;
import com.sptpc.domain.Book;
import com.sptpc.domain.BorrowBook;
import com.sptpc.domain.po.BookState;
import com.sptpc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookMapper bookMapper;

	@Override
	public List<Book> selectAll() {
		return bookMapper.selectAll();
	}

	@Override
	public List<Book> selectBookByName(String bookName) {
		return bookMapper.selectBookByName(bookName);
	}

	@Override
	public int bookingById(String uid, String bid, Date startDate, Date endDate) {
		return bookMapper.bookingById(uid,bid,startDate,endDate);
	}

	@Override
	public List<BookState> queryBorrowById(String uid) {
		return bookMapper.queryBorrowById(uid);
	}

	@Override
	public List<BookState> queryReadingListByid(String uid) {
		return bookMapper.queryReadingListByid(uid);
	}

	@Override
	public BorrowBook queryDateByUidAndBid(String uid, String bid) {
		return bookMapper.queryDateByUidAndBid(uid,bid);
	}

	@Override
	public int updateBk(BorrowBook borrowBook) {
		return bookMapper.updateBk(borrowBook);
	}

	@Override
	public int returnBookById(String uid, String bid, Date date) {
		return bookMapper.returnBookById(uid, bid, date);
	}

	@Override
	public List<BookState> queryReturnStateById(String uid) {
		return bookMapper.queryReturnStateById(uid);
	}

	@Override
	public List<BookState> queryReturnedById(String uid) {
		return bookMapper.queryReturnedById(uid);
	}

	@Override
	public List<BookState> queryReturnBookList() {
		return bookMapper.queryReturnBookList();
	}

	@Override
	public List<BookState> queryAllBookState() {
		return bookMapper.queryAllBookState();
	}

	@Override
	public List<BookState> selectBookStateByName(String bookName) {
		return bookMapper.selectBookStateByName(bookName);
	}

	@Override
	public Book selectCountByBid(String bid) {
		return bookMapper.selectCountByBid(bid);
	}
}
