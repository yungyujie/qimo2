package com.sptpc.service.impl;

import com.sptpc.dao.AdminMapper;
import com.sptpc.domain.Book;
import com.sptpc.domain.Reader;
import com.sptpc.domain.po.BookState;
import com.sptpc.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminMapper adminMapper;

    @Override
    public Reader findUserByIdAndPwd(String id, String password) {
        return adminMapper.findUserByIdAndPwd(id, password);
    }

    @Override
    public List<Reader> findAll() {
        return adminMapper.findAll();
    }

    @Override
    public List<BookState> queryCheckBookList() {
        return adminMapper.queryCheckBookList();
    }

    @Override
    public int borrowBook(String bid, String uid) {
        return adminMapper.borrowBook(bid, uid);
    }

    @Override
    public int minusBook(String bid) {
        return adminMapper.minusBook(bid);
    }

    @Override
    public int cancelBook(String uid, String bid) {
        return adminMapper.cancelBook(uid, bid);
    }

    @Override
    public int addBook(Book book) {
        return adminMapper.addBook(book);
    }

    @Override
    public int passReturn(String bid, String uid) {
        return adminMapper.passReturn(bid, uid);
    }

    @Override
    public int reset(String id, String name, String dept, String phone) {
        return adminMapper.reset(id, name, dept, phone);
    }

    @Override
    public int delUserById(String id) {
        return adminMapper.delUserById(id);
    }

    @Override
    public int addAdmin(String id) {
        return adminMapper.addAdmin(id);
    }
}
