package com.sptpc.service.impl;

import com.sptpc.dao.UserMapper;
import com.sptpc.domain.BookReview;
import com.sptpc.domain.Reader;
import com.sptpc.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserMapper userMapper;


	@Override
	public Reader findUserByIdAndPwd(String id, String password) {
		return userMapper.findUserByIdAndPwd(id , password);
	}

	@Override
	public int updateAllById(Reader reader) {
		return userMapper.updateAllById(reader);
	}

	@Override
	public int updatePwdById(String newpass, String id , String pass) {
		return userMapper.updatePwdById(newpass,id,pass);
	}

	@Override
	public int addReader(Reader reader) {
		return userMapper.addReader(reader);
	}

	@Override
	public int delReaderByUidAndPwd(String uid, String pwd){
		return userMapper.delReaderByUidAndPwd(uid,pwd);
	}

	@Override
	public int review(String bid, String uid, String grade, String desc) {
		return userMapper.review(bid,uid,grade,desc);
	}

	@Override
	public List<BookReview> queryAllReview() {
		return userMapper.queryAllReview();
	}

	@Override
	public List<BookReview> searchReview(String bookName) {
		return userMapper.searchReview(bookName);
	}

	@Override
	public List<BookReview> queryReviewById(String uid) {
		return userMapper.queryReviewById(uid);
	}
}
