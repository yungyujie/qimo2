package com.sptpc.service;

import com.sptpc.domain.Book;
import com.sptpc.domain.BorrowBook;
import com.sptpc.domain.po.BookState;

import java.util.Date;
import java.util.List;

public interface BookService {
	List<Book> selectAll();

	List<Book> selectBookByName(String bookName);

    int bookingById(String uid, String bid, Date startDate, Date endDate);

    List<BookState> queryBorrowById(String uid);

    List<BookState> queryReadingListByid(String uid);

    BorrowBook queryDateByUidAndBid(String uid, String bid);

    int updateBk(BorrowBook borrowBook);

    int returnBookById(String uid, String bid, Date date);

    List<BookState> queryReturnStateById(String uid);

    List<BookState> queryReturnedById(String uid);

    List<BookState> queryReturnBookList();

    List<BookState> queryAllBookState();

    List<BookState> selectBookStateByName(String bookName);

    Book selectCountByBid(String bid);
}
