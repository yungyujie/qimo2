package com.sptpc.service;

import com.sptpc.domain.Book;
import com.sptpc.domain.Reader;
import com.sptpc.domain.po.BookState;

import java.util.List;

public interface AdminService {
    Reader findUserByIdAndPwd(String id, String password);

    List<Reader> findAll();

    List<BookState> queryCheckBookList();

    int borrowBook(String bid, String uid);

    int minusBook(String bid);

    int cancelBook(String uid, String bid);

    int addBook(Book book);

    int passReturn(String bid, String uid);

    int reset(String id, String name, String dept, String phone);

    int delUserById(String id);

    int addAdmin(String id);
}
