package com.sptpc.controller;

import com.alibaba.druid.util.StringUtils;
import com.sptpc.domain.Book;
import com.sptpc.domain.Reader;
import com.sptpc.domain.po.BookState;
import com.sptpc.service.AdminService;
import com.sptpc.service.BookService;
import com.sptpc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    UserService userService;

    @Autowired
    AdminService adminService;

    @Autowired
    BookService bookService;

    //登录功能
    @RequestMapping("login")
    public String login(String id , String password , HttpSession session){
        Reader reader = adminService.findUserByIdAndPwd(id , password);
        if (reader != null && reader.getReaderType().equals("2")){
            session.setAttribute("session_reader" , reader);
            return "admin-home";
        }
        return "register" ;
    }

    //显示用户列表
    @RequestMapping("userList")
    public String userList(Model model) {
        List<Reader> readers = adminService.findAll();
        model.addAttribute("reader" , readers);
        return "admin-userList";
    }

    //查看审核借书
    @RequestMapping("checkBookList")
    public String checkBookList(Model model){
        List<BookState> books = adminService.queryCheckBookList();
        model.addAttribute("bookList",books);
        return "admin-checkBook";
    }

    //跳转到图书上架
    @RequestMapping("returnAddBook")
    public String returnAddBook(){
        return "admin-addBook";
    }

    //添加图书
    @RequestMapping("addBook")
    public String addBook(String bid, String type, String bname, String author,
                          String publish, String unitPrice, String count){
        Book book = new Book();
        book.setAuthor(author);
        book.setBookName(bname);
        book.setCount(Integer.parseInt(count));
        book.setISBN(bid);
        book.setPublish(publish);
        book.setTypeID(type);
        book.setUnitPrice(unitPrice);
        int count2 = adminService.addBook(book);
        if (count2 > 0){
            return "true";
        }
        return "false";
    }

    //通过审核功能
    @RequestMapping("checkBook")
    public String checkBook(String bid, String uid){
        int count = adminService.borrowBook(bid, uid);//审核借书功能
        int count2 = adminService.minusBook(bid);//图书库存自动减少一本
        if (count2 > 0 ){
            return "true";
        }
        return "false";
    }

    //取消借阅
    @RequestMapping("cancelBook")
    public String cancelBook(String uid, String bid){
        int count = adminService.cancelBook(uid, bid);
        if (count > 0){
            return "true";
        }
        return "false";
    }

    //审核还书
    @RequestMapping("returnBookList")
    public String returnBookList(Model model){
        List<BookState> books = bookService.queryReturnBookList();
        model.addAttribute("book",books);
        return "admin-returnBook";
    }

    //通过还书
    @RequestMapping("passReturn")
    public String passReturn(String bid, String uid){
        int count = adminService.passReturn(bid, uid);
//        int count2 = adminService.addBook()
        if (count > 0){
            return "true";
        }
        return "false";
    }

    //查看库存
    @RequestMapping("bookList")
    public String bookList(Model model){
        List<BookState> books = bookService.queryAllBookState();
        model.addAttribute("book", books);
        return "admin-bookList";
    }

    //搜索图书
    @RequestMapping("search")
    public String search(Model model, String bookName){
        List<BookState> books;
        if (StringUtils.isEmpty(bookName)){
            books = bookService.queryAllBookState();
        }else {
            books = bookService.selectBookStateByName(bookName);
        }
        model.addAttribute("searchBook", books);
        return "admin-searchList";
    }

    //修改用户信息
    @RequestMapping("sendId")
    public String sendId(String id, String name, String dept, String phone, Model model){
        Reader reader = new Reader();
        reader.setReaderName(name);
        reader.setReaderId(id);
        reader.setReaderDept(dept);
        reader.setReaderPhone(phone);
        model.addAttribute("reader",reader);
        return "admin-resetUser";
    }

    //修改个人信息
    @RequestMapping("readerReset")
    public String readerReset(String uid, String username, String dept, String phone, Model model){
        int count = adminService.reset(uid, username, dept, phone);
        if (count > 0){
            return "true";
        }
        return "false";
    }

    //删除用户
    @RequestMapping("delUser")
    public String delUser(String id){
        int count = adminService.delUserById(id);
        if (count > 0){
            return "true";
        }
        return "false";
    }

    //设为管理员
    @RequestMapping("addAdmin")
    public String addAdmin(String id){
        int count = adminService.addAdmin(id);
        if (count > 0){
            return "true";
        }
        return "false";
    }

}
