package com.sptpc.controller;

import com.alibaba.druid.util.StringUtils;
import com.sptpc.domain.Book;
import com.sptpc.domain.BorrowBook;
import com.sptpc.domain.po.BookState;
import com.sptpc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

	@Autowired
	BookService bookService;

	//显示所有的book
	@RequestMapping("/bookList")
	public String bookList(Model model){
		List<Book> book = bookService.selectAll();
		model.addAttribute("book" , book);
		return "book-list";
	}

	//搜索图书
	@RequestMapping("/search")
	public String search(String bookName, Model model){
		List<BookState> books;
		if (StringUtils.isEmpty(bookName)){
			books = bookService.queryAllBookState();
		}else {
			books = bookService.selectBookStateByName(bookName);
		}
		model.addAttribute("searchBook", books);
		return "searchList";
	}

	//预约借书
	@RequestMapping("/booking")
	public String booking(String uid, String bid){
		Book book = bookService.selectCountByBid(bid);
		int bookCount = book.getCount();
		if (bookCount <= 0){
			return "bookNull";
		}
		Date startDate = new Date();//借书日期为当前日期
		Calendar calendar=Calendar.getInstance();//为当前日期添加15天
		calendar.setTime(startDate);
		calendar.add(Calendar.DATE, 15);
		Date endData=calendar.getTime();//将处理好的日期保存
		int count = bookService.bookingById(uid,bid,startDate,endData);
		if (count > 0){
			return "true";
		}
		return "false";
	}

	//查看借书状态
	@RequestMapping("/bookState")
	public String bookState(String uid, Model model){
		List<BookState> books = bookService.queryBorrowById(uid);
		model.addAttribute("book",books);
		return "state";
	}

	//查看我的图书
	@RequestMapping("readingList")
	public String readingList(String uid, Model model){
		List<BookState> books = bookService.queryReadingListByid(uid);
		model.addAttribute("book", books);
		return "reading-list";
	}

	//图书续期
	@RequestMapping("renew")
	public String renew(String uid, String bid){
		BorrowBook borrowBook = bookService.queryDateByUidAndBid(uid,bid);
		Calendar calendar=Calendar.getInstance();//为当前日期添加15天
		calendar.setTime(borrowBook.getReturnDate());
		calendar.add(Calendar.DATE, 15);
		Date newDate=calendar.getTime();//将处理好的日期保存
		borrowBook.setReturnDate(newDate);//将新的时间保存
		int count = bookService.updateBk(borrowBook);
		if (count > 0){
			return "true";
		}
		return "false";
	}

	//还书
	@RequestMapping("returnBook")
	public String returnBook(String uid, String bid){
		Date date = new Date();
		int count = bookService.returnBookById(uid, bid, date);
		if (count > 0){
			return "true";
		}
		return "false";
	}

	//查看待审核图书
	@RequestMapping("returnState")
	public String returnState(String uid, Model model){
		List<BookState> books = bookService.queryReturnStateById(uid);
		model.addAttribute("book", books);
		return "return-state";
	}

	//查看已还图书
	@RequestMapping("returned")
	public String returned(String uid, Model model){
		List<BookState> books = bookService.queryReturnedById(uid);
		model.addAttribute("book", books);
		return "returned-book";
	}


}
