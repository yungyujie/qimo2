package com.sptpc.controller;

import com.alibaba.druid.util.StringUtils;
import com.sptpc.domain.Book;
import com.sptpc.domain.BookReview;
import com.sptpc.domain.Reader;
import com.sptpc.domain.po.BookState;
import com.sptpc.service.BookService;
import com.sptpc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("user/")
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	BookService bookService;

	//登录功能
	@RequestMapping("login.do")
	public String login(String id , String password , HttpSession session){
		Reader reader = userService.findUserByIdAndPwd(id , password);
		if (reader != null && reader.getReaderType().equals("1")){
			session.setAttribute("session_reader" , reader);
			return "home";
		}
		return "register" ;
	}

	//退出登陆
	@RequestMapping("back.do")
	public String back(HttpSession session){
		if (session.getAttribute("session_reader") != null){
			session.removeAttribute("session_reader");
			return "redirect:/login";
		}
		return "false";
	}

	//修改用户信息
	@RequestMapping("readerReset.do")
	public String readerReset(String username ,
	                          String dept, String phone,
	                          HttpSession session){
		System.out.println(username+" "+ dept +" " +phone);
		Reader reader = (Reader) session.getAttribute("session_reader");
		reader.setReaderName(username);
		reader.setReaderPhone(phone);
		reader.setReaderDept(dept);
		System.out.println(reader.toString());
		int count = userService.updateAllById(reader);
		if (count > 0){
			return "redirect:/resetList";
		}
		return "false";
	}

	//修改密码
	@RequestMapping("resetPwd.do")
	public String resetPwd(String pass , String newpass ,HttpSession session){
		Reader reader = (Reader) session.getAttribute("session_reader");
		String id = reader.getReaderId();
		int count = userService.updatePwdById(newpass , id , pass);
		System.out.println(reader.toString() + "  " + pass);
		if (count > 0){
			return "resetPwd";
		}
		return "false";
	}

	//注册功能
	@RequestMapping("register.do")
	public String register(String userid , String type ,
	                       String username , String password ,
	                       String dept , String phone){
		Reader reader = new Reader();
		reader.setReaderId(userid);
		reader.setReaderName(username);
		reader.setReaderPwd(password);
		reader.setReaderDept(dept);
		reader.setReaderPhone(phone);
		if (type.equals("user")){
			reader.setReaderType("1");//1 用户
		}else {
			reader.setReaderType("2");//2 管理员
		}
		int count = userService.addReader(reader);
		if (count > 0){
			return "login";
		}
		return "false";
	}

	//跳转到注销账号界面
	@RequestMapping("deletReader")
	public String deletReader(Model model){
		return "deletReader";
	}

	//注销用户
	@RequestMapping("delReader")
	public String delReader(HttpSession session,Model model , String uid , String pass){
		int count = userService.delReaderByUidAndPwd(uid,pass);
		if (count > 0){
		    return "true";
        }
		return "false";
	}

	//我要评价
	@RequestMapping("wantSay")
	public String wantSay(String uid, Model model){
		List<BookState> books = bookService.queryReturnedById(uid);
		model.addAttribute("book", books);
		return "wantToSay";
	}

	//评价图书
	@RequestMapping("wantRv")
	public String wantRv(String name, String bid, Model model){
		Book book = new Book();
		book.setBookName(name);
		book.setISBN(bid);
		model.addAttribute("book", book);
		return "review";
	}

	//保存评价图书
	@RequestMapping("review")
	public String review(String bid, String uid, String grade, String desc){

		int count = userService.review(bid,uid,grade,desc);
		if (count > 0){
			return "true";
		}
		return "false";
	}

	//大家评价
	@RequestMapping("comReview")
	public String comReview(Model model){
		List<BookReview> reviews = userService.queryAllReview();
		model.addAttribute("review", reviews);
		return "comReview";
	}

	//查询书评
	@RequestMapping("searchReview")
	public String searchReview(String bookName, Model model){
		List<BookReview> bookReviews;
		if (StringUtils.isEmpty(bookName)){
			bookReviews = userService.queryAllReview();
		}else {
			bookReviews = userService.searchReview(bookName);
		}
		model.addAttribute("review",bookReviews);
		return  "search-review";
	}

	//我的评价
	@RequestMapping("myReview")
	public String myReview(String uid, Model model){
		List<BookReview> reviews = userService.queryReviewById(uid);
		model.addAttribute("review",reviews);
		return "myReview";
	}
}
