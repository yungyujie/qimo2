package com.sptpc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    @RequestMapping("/info")
    public String info(){
        return "info";
    }

    @RequestMapping("/main")
    public String main(){
        return "main";
    }

    @RequestMapping("/menu")
    public String menu(){
        return "menu";
    }

    @RequestMapping("/top")
    public String top(){
        return "top";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/welcome.do")
    public String welcome(){
        return "welcome";
    }

    @RequestMapping("/readerReset")
    public String readerReset(){
        return "readerReset";
    }

    @RequestMapping("/resetPwd")
    public String resetPwd(){
        return "resetPwd";
    }

    @RequestMapping("/resetList")
    public String resetList(){
        return "resetList";
    }

    @RequestMapping("/register")
    public String register(){
        return "register";
    }

    @RequestMapping("/admin-login")
    public String adminLogin(){
        return "admin-login";
    }





}
