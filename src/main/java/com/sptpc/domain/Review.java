package com.sptpc.domain;

public class Review {
    private int ID ;
    private String ISBN;
    private String ReaderID;
    private String Grade;
    private String Talk;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getReaderID() {
        return ReaderID;
    }

    public void setReaderID(String readerID) {
        ReaderID = readerID;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getTalk() {
        return Talk;
    }

    public void setTalk(String talk) {
        Talk = talk;
    }
}
