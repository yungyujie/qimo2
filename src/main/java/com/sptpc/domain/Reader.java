package com.sptpc.domain;

public class Reader {
	private String readerId; 
	private String readerType; 
	private String readerName; 
	private String readerPwd; 
	private String readerDept; 
	private String readerShow; 
	private String readerPhone;
		
	public String getReaderId() {
		return readerId;
	}
	public void setReaderId(String readerId) {
		this.readerId = readerId;
	}
	public String getReaderType() {
		return readerType;
	}
	public void setReaderType(String readerType) {
		this.readerType = readerType;
	}
	public String getReaderName() {
		return readerName;
	}
	public void setReaderName(String readerName) {
		this.readerName = readerName;
	}
	public String getReaderPwd() {
		return readerPwd;
	}
	public void setReaderPwd(String readerPwd) {
		this.readerPwd = readerPwd;
	}
	public String getReaderDept() {
		return readerDept;
	}
	public void setReaderDept(String readerDept) {
		this.readerDept = readerDept;
	}
	public String getReaderShow() {
		return readerShow;
	}
	public void setReaderShow(String readerShow) {
		this.readerShow = readerShow;
	}
	public String getReaderPhone() {
		return readerPhone;
	}
	public void setReaderPhone(String readerPhone) {
		this.readerPhone = readerPhone;
	}

	@Override
	public String toString() {
		return "Reader{" +
				"readerId='" + readerId + '\'' +
				", readerType='" + readerType + '\'' +
				", readerName='" + readerName + '\'' +
				", readerPwd='" + readerPwd + '\'' +
				", readerDept='" + readerDept + '\'' +
				", readerShow='" + readerShow + '\'' +
				", readerPhone=" + readerPhone +
				'}';
	}
}
