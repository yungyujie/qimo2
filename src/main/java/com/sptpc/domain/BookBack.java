package com.sptpc.domain;

import java.util.Date;

public class BookBack {
    private int Id;
    private String ReaderID;
    private String ISBN;
    private Date ReturnDate;
    private String State;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getReaderID() {
        return ReaderID;
    }

    public void setReaderID(String readerID) {
        ReaderID = readerID;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public Date getReturnDate() {
        return ReturnDate;
    }

    public void setReturnDate(Date returnDate) {
        ReturnDate = returnDate;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }
}
