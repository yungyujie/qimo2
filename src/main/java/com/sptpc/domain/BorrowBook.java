package com.sptpc.domain;

import java.util.Date;

public class BorrowBook {
	private String ReaderID;
	private String ISBN;
	private Date BorrowDate;
	private Date ReturnDate;
	private String State;

	public String getReaderID() {
		return ReaderID;
	}

	public void setReaderID(String readerID) {
		ReaderID = readerID;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}

	public Date getBorrowDate() {
		return BorrowDate;
	}

	public void setBorrowDate(Date borrowDate) {
		BorrowDate = borrowDate;
	}

	public Date getReturnDate() {
		return ReturnDate;
	}

	public void setReturnDate(Date returnDate) {
		ReturnDate = returnDate;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}
}
