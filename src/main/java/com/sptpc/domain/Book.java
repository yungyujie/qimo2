package com.sptpc.domain;

public class Book {
	private String ISBN ;
	private String typeID ;
	private String bookName ;
	private String author ;
	private String publish ;
	private String unitPrice ;
	private int count ;
	
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public String getTypeID() {
		return typeID;
	}
	public void setTypeID(String typeName) {
		this.typeID = typeName;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublish() {
		return publish;
	}
	public void setPublish(String pulish) {
		this.publish = pulish;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
