<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
  </head>
  
  <body>
<%--<%--%>
<%--	Reader reader = (Reader)session.getAttribute("reader");--%>
<%--%>--%>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>IBSN</th>
            <th>读书名称</th>
            <th>作者</th>
            <th>借书日期</th>
            <th>还书日期</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
          <c:forEach items="${book}" var="book" >
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td>${book.ISBN}</td>
            <td>${book.bookName}</td>
            <td>${book.author}</td>
            <td>${book.borrowDate}</td>
            <td>${book.returnDate}</td>
            <td class="td-manage">
              <a title="我要续期"  href="/book/renew?uid=${book.readerID}&bid=${book.ISBN}">
                <i class="layui-icon">&#xe63c;</i>我要续期
              </a>
               <a title="我要还书" style="margin-left: 20px"  href="/book/returnBook?uid=${book.readerID}&bid=${book.ISBN}">
                <i class="layui-icon">&#xe618;</i>我要还书
              </a>
            </td>
          </tr>
<%--          <%--%>
<%--        		}--%>
<%--        	}--%>
<%--          %>--%>
          </c:forEach>
        </tbody>
      </table>
    </div>   
  </body>
</html>